<?php

require_once '../vendor/common.php';

const BASE_URL = 'http://localhost:8080';

class Ex1Tests extends WebTestCaseExtended {

    function indexToA() {
        $this->fetch('/');

        $this->ensureRelativeLink("a.html");
        $this->clickLink("a.html");
        $this->assertCurrentUrlEndsWith("a/a.html");
    }

    function aToE() {
        $this->fetch('/a/a.html');

        $this->ensureRelativeLink("e.html");
        $this->clickLink("e.html");
        $this->assertCurrentUrlEndsWith("a/b/c/d/e/e.html");
    }

    function eToD() {
        $this->fetch('/a/b/c/d/e/e.html');

        $this->ensureRelativeLink("d.html");
        $this->clickLink("d.html");
        $this->assertCurrentUrlEndsWith("a/b/c/d/d.html");
    }

    function dToE() {
        $this->fetch('/a/b/c/d/d.html');

        $this->ensureRelativeLink("e.html");
        $this->clickLink("e.html");
        $this->assertCurrentUrlEndsWith("a/b/c/d/e/e.html");
    }

    function eToB() {
        $this->fetch('/a/b/c/d/e/e.html');

        $this->ensureRelativeLink("b.html");
        $this->clickLink("b.html");
        $this->assertCurrentUrlEndsWith("a/b/b.html");
    }

    private function fetch($url) {
        $this->get(BASE_URL . $url);
    }

}

(new Ex1Tests())->run(new PointsReporter(6,
    [1 => 1,
     2 => 2,
     3 => 3,
     4 => 4,
     5 => 6]));
